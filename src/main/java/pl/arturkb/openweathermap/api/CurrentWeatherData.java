package pl.arturkb.openweathermap.api;

import pl.arturkb.openweathermap.dto.CurrentWeatherReport;

import java.io.IOException;
import java.util.Optional;

public interface CurrentWeatherData {

    /**
     * Getting current weather report by geographic coordinates.
     *
     * @param latitude  the latitude
     * @param longitude the longitude
     * @return current weather report when no error or empty Optional. .
     * @throws IOException throw when deserialization error occurs
     */
    Optional<CurrentWeatherReport> byGeographicCoordinates(float latitude, float longitude) throws IOException;


    /**
     * You can call by city name or city name and country code.
     * API responds with a Optional of results that match a searching word.
     *
     * @param cityName    the city name to search weather data for
     * @param countryCode country code to use together with city name
     * @return Optional that match a searching word, or empty Optional.
     * @throws IOException throw when deserialization error occurs
     */
    Optional<CurrentWeatherReport> byCityName(String cityName, String countryCode) throws IOException;

}
