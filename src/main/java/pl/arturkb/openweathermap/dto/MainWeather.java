package pl.arturkb.openweathermap.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.Objects;

@JsonDeserialize(builder = MainWeather.Builder.class)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MainWeather {

    /**
     * Temperature. Unit Default: Kelvin, Metric: Celsius, Imperial: Fahrenheit.
     */
    private final float temperature;

    /**
     * Atmospheric pressure (on the sea level, if there is no sea_level or ground_level data), hPa
     */
    private final float pressure;

    /**
     * Humidity, %
     */
    private final float humidity;

    /**
     * Minimum temperature at the moment. This is deviation from current temp that is possible for large cities and
     * megalopolises geographically expanded (use these parameter optionally).
     * Unit Default: Kelvin, Metric: Celsius, Imperial: Fahrenheit.
     */
    private final float minimumTemperatureAtTheMoment;

    /**
     * Maximum temperature at the moment. This is deviation from current temp that is possible for large cities and
     * megalopolises geographically expanded (use these parameter optionally).
     * Unit Default: Kelvin, Metric: Celsius, Imperial: Fahrenheit.
     */
    private final float maximumTemperatureAtTheMoment;

    /**
     * Atmospheric pressure on the sea level, hPa
     */
    private final float atmosphericPressureOnTheSeaLevel;

    /**
     * Atmospheric pressure on the ground level, hPa
     */
    private final float atmosphericPressureOnTheGroundLevel;

    private MainWeather(Builder builder) {
        this.temperature = builder.temperature;
        this.pressure = builder.pressure;
        this.humidity = builder.humidity;
        this.minimumTemperatureAtTheMoment = builder.minimumTemperatureAtTheMoment;
        this.maximumTemperatureAtTheMoment = builder.maximumTemperatureAtTheMoment;
        this.atmosphericPressureOnTheSeaLevel = builder.atmosphericPressureOnTheSeaLevel;
        this.atmosphericPressureOnTheGroundLevel = builder.atmosphericPressureOnTheGroundLevel;
    }

    public static Builder make() {
        return new Builder();
    }

    public static Builder copy(MainWeather other) {
        return new Builder(other);
    }

    @SuppressWarnings("WeakerAccess")
    @JsonProperty("temp")
    public float getTemperature() {
        return temperature;
    }

    @SuppressWarnings("WeakerAccess")
    @JsonProperty("pressure")
    public float getPressure() {
        return pressure;
    }

    @SuppressWarnings("WeakerAccess")
    @JsonProperty("humidity")
    public float getHumidity() {
        return humidity;
    }

    @SuppressWarnings("WeakerAccess")
    @JsonProperty("temp_min")
    public float getMinimumTemperatureAtTheMoment() {
        return minimumTemperatureAtTheMoment;
    }

    @SuppressWarnings("WeakerAccess")
    @JsonProperty("temp_max")
    public float getMaximumTemperatureAtTheMoment() {
        return maximumTemperatureAtTheMoment;
    }

    @SuppressWarnings("WeakerAccess")
    @JsonProperty("sea_level")
    public float getAtmosphericPressureOnTheSeaLevel() {
        return atmosphericPressureOnTheSeaLevel;
    }

    @SuppressWarnings({"WeakerAccess", "SpellCheckingInspection"})
    @JsonProperty("grnd_level")
    public float getAtmosphericPressureOnTheGroundLevel() {
        return atmosphericPressureOnTheGroundLevel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MainWeather)) return false;
        MainWeather that = (MainWeather) o;
        return Float.compare(that.temperature, temperature) == 0 &&
                Float.compare(that.pressure, pressure) == 0 &&
                Float.compare(that.humidity, humidity) == 0 &&
                Float.compare(that.minimumTemperatureAtTheMoment, minimumTemperatureAtTheMoment) == 0 &&
                Float.compare(that.maximumTemperatureAtTheMoment, maximumTemperatureAtTheMoment) == 0 &&
                Float.compare(that.atmosphericPressureOnTheSeaLevel, atmosphericPressureOnTheSeaLevel) == 0 &&
                Float.compare(that.atmosphericPressureOnTheGroundLevel, atmosphericPressureOnTheGroundLevel) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(temperature, pressure, humidity, minimumTemperatureAtTheMoment,
                maximumTemperatureAtTheMoment, atmosphericPressureOnTheSeaLevel, atmosphericPressureOnTheGroundLevel);
    }

    public static class Builder {
        private float temperature;
        private float pressure;
        private float humidity;
        private float minimumTemperatureAtTheMoment;
        private float maximumTemperatureAtTheMoment;
        private float atmosphericPressureOnTheSeaLevel;
        private float atmosphericPressureOnTheGroundLevel;

        Builder() {
        }

        Builder(MainWeather other) {
            if (other == null) {
                throw new NullPointerException("Can't copy from null: MainWeather.Builder");
            } else {
                this.temperature = other.getTemperature();
                this.pressure = other.getPressure();
                this.humidity = other.getHumidity();
                this.minimumTemperatureAtTheMoment = other.getMinimumTemperatureAtTheMoment();
                this.maximumTemperatureAtTheMoment = other.getMaximumTemperatureAtTheMoment();
                this.atmosphericPressureOnTheSeaLevel = other.getAtmosphericPressureOnTheSeaLevel();
                this.atmosphericPressureOnTheGroundLevel = other.getAtmosphericPressureOnTheGroundLevel();
            }
        }

        @SuppressWarnings("WeakerAccess")
        @JsonProperty("temp")
        public Builder withTemperature(float temperature) {
            this.temperature = temperature;
            return this;
        }

        @SuppressWarnings("WeakerAccess")
        @JsonProperty("pressure")
        public Builder withPressure(float pressure) {
            this.pressure = pressure;
            return this;
        }

        @SuppressWarnings("WeakerAccess")
        @JsonProperty("humidity")
        public Builder withHumidity(float humidity) {
            this.humidity = humidity;
            return this;
        }

        @SuppressWarnings("WeakerAccess")
        @JsonProperty("temp_min")
        public Builder withMinimumTemperatureAtTheMoment(float minimumTemperatureAtTheMoment) {
            this.minimumTemperatureAtTheMoment = minimumTemperatureAtTheMoment;
            return this;
        }

        @SuppressWarnings("WeakerAccess")
        @JsonProperty("temp_max")
        public Builder withMaximumTemperatureAtTheMoment(float maximumTemperatureAtTheMoment) {
            this.maximumTemperatureAtTheMoment = maximumTemperatureAtTheMoment;
            return this;
        }

        @SuppressWarnings({"WeakerAccess", "unused"})
        @JsonProperty("sea_level")
        public Builder withAtmosphericPressureOnTheSeaLevel(float atmosphericPressureOnTheSeaLevel) {
            this.atmosphericPressureOnTheSeaLevel = atmosphericPressureOnTheSeaLevel;
            return this;
        }

        @SuppressWarnings({"WeakerAccess", "SpellCheckingInspection", "unused"})
        @JsonProperty("grnd_level")
        public Builder withAtmosphericPressureOnTheGroundLevel(float atmosphericPressureOnTheGroundLevel) {
            this.atmosphericPressureOnTheGroundLevel = atmosphericPressureOnTheGroundLevel;
            return this;
        }

        public MainWeather build() {
            return new MainWeather(this);
        }
    }

}
