package pl.arturkb.openweathermap.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.type.TypeFactory;

import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

/**
 * Mapping utils class.
 */
public class MappingUtils {

    public static <T> String getJson(T object) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(object);
    }

    public static <T> List<String> getJsons(List<T> objects) throws JsonProcessingException {
        List<String> result = new ArrayList<>(objects.size());

        for (T object : objects) {
            result.add(getJson(object));
        }

        return result;
    }

    public static <T> T getObject(String json, Class<T> klass) throws IOException {
        return new ObjectMapper().readValue(json, klass);
    }

    public static <T> T getObject(Reader json, Class<T> klass) throws IOException {
        return new ObjectMapper().readValue(json, klass);
    }

    public static <T> List<T> getObjects(String json, Class<T> klass) throws IOException {
        TypeFactory typeFactory = TypeFactory.defaultInstance();

        CollectionType valueType = typeFactory.constructCollectionType(ArrayList.class, klass);
        return new ObjectMapper().readValue(json, valueType);
    }

    public static <T> List<T> getObjects(List<String> jsons, Class<T> klass) throws IOException {
        List<T> result = new ArrayList<>(jsons.size());

        for (String json : jsons) {
            result.add(getObject(json, klass));
        }

        return result;
    }

}
