package pl.arturkb.openweathermap.api;

import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import org.junit.Rule;
import org.junit.Test;
import pl.arturkb.openweathermap.OpenWeatherMapClient;
import pl.arturkb.openweathermap.OpenWeatherMapClientFactory;
import pl.arturkb.openweathermap.dto.CurrentWeatherReport;
import pl.arturkb.openweathermap.utils.MappingUtils;

import java.io.IOException;
import java.util.Optional;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.hamcrest.MatcherAssert.assertThat;

public class CurrentWeatherDataTest {

    private static final String HOST = "localhost:8080";
    private static final String BASE_URI = "/data/2.5/weather";
    private static final String API_KEY_PREFIX = "&APPID=";
    private static final String API_KEY = "123456789";
    private static final String LONDON = "London";
    private static final String COUNTRY_CODE = ",no";
    private static final String QUERY_BY_CITYNAME = "?q=";
    private static final String QUERY_BY_GEO_LOCATION_LAT = "?lat=";
    private static final String QUERY_BY_GEO_LOCATION_LON = "&lon=";
    private static final String USE_METRIC = "&units=metric";


    private final OpenWeatherMapClient openWeatherMapClient = OpenWeatherMapClientFactory.getInstance(HOST, API_KEY);

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(WireMockConfiguration.options().port(8080).bindAddress("localhost"));

    @Test
    public void syncByCityNameHappyDay() throws IOException {

        stubFor(get(anyUrl())
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(exampleResponse())));

        final CurrentWeatherData currentWeatherData = new CurrentWeatherDataSync(openWeatherMapClient, true);
        final Optional<CurrentWeatherReport> currentWeatherReport = currentWeatherData.byCityName("London", "no");
        final CurrentWeatherReport exampleObject = MappingUtils.getObject(exampleResponse(), CurrentWeatherReport.class);
        //noinspection OptionalGetWithoutIsPresent
        assertThat(currentWeatherReport.get(), org.hamcrest.CoreMatchers.equalTo(exampleObject));
    }

    @SuppressWarnings("StringBufferReplaceableByString")
    @Test
    public void syncByCityNameUrlMatchByCityNameAndCountryCode() throws IOException {

        String testUrl = new StringBuilder()
                .append(BASE_URI)
                .append(QUERY_BY_CITYNAME)
                .append(LONDON)
                .append(COUNTRY_CODE)
                .append(USE_METRIC)
                .append(API_KEY_PREFIX)
                .append(API_KEY)
                .toString();
        stubFor(get(urlEqualTo(testUrl))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(exampleResponse())));

        final CurrentWeatherData currentWeatherData = new CurrentWeatherDataSync(openWeatherMapClient, true);
        final Optional<CurrentWeatherReport> currentWeatherReport = currentWeatherData.byCityName("London", "no");
        final CurrentWeatherReport exampleObject = MappingUtils.getObject(exampleResponse(), CurrentWeatherReport.class);
        //noinspection OptionalGetWithoutIsPresent
        assertThat(currentWeatherReport.get(), org.hamcrest.CoreMatchers.equalTo(exampleObject));
    }

    @SuppressWarnings("StringBufferReplaceableByString")
    @Test
    public void syncByCityNameUrlMatchByCityName() throws IOException {

        String testUrl = new StringBuilder()
                .append(BASE_URI)
                .append(QUERY_BY_CITYNAME)
                .append(LONDON)
                .append(USE_METRIC)
                .append(API_KEY_PREFIX)
                .append(API_KEY)
                .toString();
        stubFor(get(urlEqualTo(testUrl))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(exampleResponse())));

        final CurrentWeatherData currentWeatherData = new CurrentWeatherDataSync(openWeatherMapClient, true);
        final Optional<CurrentWeatherReport> currentWeatherReport = currentWeatherData.byCityName("London", null);
        final CurrentWeatherReport exampleObject = MappingUtils.getObject(exampleResponse(), CurrentWeatherReport.class);
        //noinspection OptionalGetWithoutIsPresent
        assertThat(currentWeatherReport.get(), org.hamcrest.CoreMatchers.equalTo(exampleObject));
    }

    @Test
    public void syncByCityCityNotFound() throws IOException {

        stubFor(get(anyUrl())
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(cityNotFound())));

        final CurrentWeatherData currentWeatherData = new CurrentWeatherDataSync(openWeatherMapClient, true);
        final Optional<CurrentWeatherReport> currentWeatherReport = currentWeatherData.byCityName("notFound", "no");
        final CurrentWeatherReport exampleObject = MappingUtils.getObject(cityNotFound(), CurrentWeatherReport.class);
        //noinspection OptionalGetWithoutIsPresent
        assertThat(currentWeatherReport.get(), org.hamcrest.CoreMatchers.equalTo(exampleObject));
    }

    @Test
    public void syncByCityName400() throws IOException {

        stubFor(get(anyUrl())
                .willReturn(aResponse()
                        .withStatus(400)
                ));

        final CurrentWeatherData currentWeatherData = new CurrentWeatherDataSync(openWeatherMapClient, true);
        final Optional<CurrentWeatherReport> currentWeatherReport = currentWeatherData.byCityName("test", "no");
        assertThat(currentWeatherReport.isEmpty(), org.hamcrest.CoreMatchers.is(true));
    }


    @Test
    public void syncByCityNameBlankCityName() throws IOException {
        final CurrentWeatherData currentWeatherData = new CurrentWeatherDataSync(openWeatherMapClient, true);
        final Optional<CurrentWeatherReport> currentWeatherReport = currentWeatherData.byCityName("", "no");
        assertThat(currentWeatherReport.isEmpty(), org.hamcrest.CoreMatchers.is(true));
    }

    @Test
    public void syncByCityNameNullName() throws IOException {
        final CurrentWeatherData currentWeatherData = new CurrentWeatherDataSync(openWeatherMapClient, true);
        final Optional<CurrentWeatherReport> currentWeatherReport = currentWeatherData.byCityName(null, "no");
        assertThat(currentWeatherReport.isEmpty(), org.hamcrest.CoreMatchers.is(true));
    }


    @SuppressWarnings("OptionalGetWithoutIsPresent")
    @Test
    public void syncByGeographicCoordinatesHappyDay() throws IOException {

        float lat = 59.74F;
        float lon = 11.29F;

        stubFor(get(anyUrl())
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(exampleResponse())));

        final CurrentWeatherData currentWeatherData = new CurrentWeatherDataSync(openWeatherMapClient, true);
        final CurrentWeatherReport currentWeatherReport = currentWeatherData.byGeographicCoordinates(lat, lon).get();
        final CurrentWeatherReport exampleObject = MappingUtils.getObject(exampleResponse(), CurrentWeatherReport.class);
        assertThat(currentWeatherReport, org.hamcrest.CoreMatchers.equalTo(exampleObject));
    }


    @SuppressWarnings({"OptionalGetWithoutIsPresent", "StringBufferReplaceableByString"})
    @Test
    public void syncByGeographicCoordinatesUrlMatch() throws IOException {

        float lat = 59.74F;
        float lon = 11.29F;

        String testUrl = new StringBuilder()
                .append(BASE_URI)
                .append(QUERY_BY_GEO_LOCATION_LAT)
                .append(lat)
                .append(QUERY_BY_GEO_LOCATION_LON)
                .append(lon)
                .append(USE_METRIC)
                .append(API_KEY_PREFIX)
                .append(API_KEY)
                .toString();
        stubFor(get(urlEqualTo(testUrl))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(exampleResponse())));

        final CurrentWeatherData currentWeatherData = new CurrentWeatherDataSync(openWeatherMapClient, true);
        final CurrentWeatherReport currentWeatherReport = currentWeatherData.byGeographicCoordinates(lat, lon).get();
        final CurrentWeatherReport exampleObject = MappingUtils.getObject(exampleResponse(), CurrentWeatherReport.class);
        assertThat(currentWeatherReport, org.hamcrest.CoreMatchers.equalTo(exampleObject));
    }

    @Test
    public void syncByGeographicCoordinates400() throws IOException {

        float lat = 59.74F;
        float lon = 11.29F;

        stubFor(get(anyUrl())
                .willReturn(aResponse()
                        .withStatus(400)
                ));

        final CurrentWeatherData currentWeatherData = new CurrentWeatherDataSync(openWeatherMapClient, true);
        final Optional<CurrentWeatherReport> currentWeatherReport = currentWeatherData.byGeographicCoordinates(lat, lon);
        assertThat(currentWeatherReport.isEmpty(), org.hamcrest.CoreMatchers.is(true));
    }

    private String cityNotFound() {
        return "{\n" +
                "    \"cod\": \"404\",\n" +
                "    \"message\": \"city not found\"\n" +
                "}";
    }

    @SuppressWarnings("UnnecessaryLocalVariable")
    private String exampleResponse() {
        String response = "{\n" +
                "    \"coord\": {\n" +
                "        \"lon\": 11.29,\n" +
                "        \"lat\": 59.74\n" +
                "    },\n" +
                "    \"weather\": [\n" +
                "        {\n" +
                "            \"id\": 500,\n" +
                "            \"main\": \"Rain\",\n" +
                "            \"description\": \"light rain\",\n" +
                "            \"icon\": \"10d\"\n" +
                "        }\n" +
                "    ],\n" +
                "    \"base\": \"stations\",\n" +
                "    \"main\": {\n" +
                "        \"temp\": 11.84,\n" +
                "        \"pressure\": 1006,\n" +
                "        \"humidity\": 93,\n" +
                "        \"temp_min\": 11,\n" +
                "        \"temp_max\": 12.78\n" +
                "    },\n" +
                "    \"wind\": {\n" +
                "        \"speed\": 6.2,\n" +
                "        \"deg\": 200\n" +
                "    },\n" +
                "    \"clouds\": {\n" +
                "        \"all\": 100\n" +
                "    },\n" +
                "    \"dt\": 1559232354,\n" +
                "    \"sys\": {\n" +
                "        \"type\": 1,\n" +
                "        \"id\": 1662,\n" +
                "        \"message\": 0.0055,\n" +
                "        \"country\": \"NO\",\n" +
                "        \"sunrise\": 1559182218,\n" +
                "        \"sunset\": 1559247282\n" +
                "    },\n" +
                "    \"timezone\": 7200,\n" +
                "    \"id\": 7626324,\n" +
                "    \"name\": \"Kirkebygda\",\n" +
                "    \"cod\": 200\n" +
                "}";
        return response;
    }

}
