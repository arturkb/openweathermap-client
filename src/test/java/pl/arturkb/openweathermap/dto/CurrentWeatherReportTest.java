package pl.arturkb.openweathermap.dto;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.json.JSONException;
import org.junit.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import pl.arturkb.openweathermap.utils.MappingUtils;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;

public class CurrentWeatherReportTest {

    @Test
    public void testObjectCopy() {
        CurrentWeatherReport currentWeatherReportOriginal = getExampleObject();

        CurrentWeatherReport copy = CurrentWeatherReport.copy(currentWeatherReportOriginal).build();
        assertThat(currentWeatherReportOriginal, equalTo(copy));

        //noinspection SpellCheckingInspection
        final CurrentWeatherReport mutation = CurrentWeatherReport.copy(copy).withCityName("Tuszyn").build();
        assertThat(currentWeatherReportOriginal, not(equalTo(mutation)));

    }

    @Test
    public void emptyReportGetJsonTest() throws JsonProcessingException, JSONException {
        String jsonExpected = "{}";

        CurrentWeatherReport currentWeatherReport = CurrentWeatherReport.make().build();
        String json = MappingUtils.getJson(currentWeatherReport);
        JSONAssert.assertEquals(jsonExpected, json, JSONCompareMode.LENIENT);
    }

    @Test
    public void emptyReportGetObjectTest() throws IOException {
        String currentWeatherReportJson = "{}";
        CurrentWeatherReport currentWeatherReportExpected = CurrentWeatherReport.make().build();

        CurrentWeatherReport currentWeatherReport = MappingUtils.getObject(currentWeatherReportJson, CurrentWeatherReport.class);
        assertThat(currentWeatherReport, equalTo(currentWeatherReportExpected));
    }

    @Test
    public void cityNameIncludedInReportGetJsonTest() throws JsonProcessingException, JSONException {
        String jsonExpected = "{\"name\": \"London\"}";
        CurrentWeatherReport currentWeatherReport = CurrentWeatherReport.make().withCityName("London").build();
        String json = MappingUtils.getJson(currentWeatherReport);
        JSONAssert.assertEquals(jsonExpected, json, JSONCompareMode.LENIENT);
    }

    @Test
    public void cityNameIncludedInReportGetObjectTest() throws IOException {
        String currentWeatherReportJson = "{\"name\": \"London\"}";
        CurrentWeatherReport currentWeatherReportExpected = CurrentWeatherReport.make().withCityName("London").build();

        CurrentWeatherReport currentWeatherReport = MappingUtils.getObject(currentWeatherReportJson, CurrentWeatherReport.class);
        assertThat(currentWeatherReport, equalTo(currentWeatherReportExpected));
    }

    @Test
    public void cityIdIncludedInReportGetJsonTest() throws JsonProcessingException, JSONException {
        String jsonExpected = "{\"id\": 2643743}";
        CurrentWeatherReport currentWeatherReport = CurrentWeatherReport.make().withCityId(2643743L).build();
        String json = MappingUtils.getJson(currentWeatherReport);
        JSONAssert.assertEquals(jsonExpected, json, JSONCompareMode.LENIENT);
    }

    @Test
    public void cityIdIncludedInReportGetObjectTest() throws IOException {
        String currentWeatherReportJson = "{\"id\": 2643743}";
        CurrentWeatherReport currentWeatherReportExpected = CurrentWeatherReport.make().withCityId(2643743L).build();

        CurrentWeatherReport currentWeatherReport = MappingUtils.getObject(currentWeatherReportJson, CurrentWeatherReport.class);
        assertThat(currentWeatherReport, equalTo(currentWeatherReportExpected));
    }

    @Test
    public void cosIncludedInReportGetJsonTest() throws JsonProcessingException, JSONException {
        String jsonExpected = "{\"cod\": 200}";
        CurrentWeatherReport currentWeatherReport = CurrentWeatherReport.make().withCod(200L).build();
        String json = MappingUtils.getJson(currentWeatherReport);
        JSONAssert.assertEquals(jsonExpected, json, JSONCompareMode.LENIENT);
    }

    @Test
    public void cosIncludedInReportGetObjectTest() throws IOException {
        String currentWeatherReportJson = "{\"cod\": 200}";
        CurrentWeatherReport currentWeatherReportExpected = CurrentWeatherReport.make().withCod(200L).build();

        CurrentWeatherReport currentWeatherReport = MappingUtils.getObject(currentWeatherReportJson, CurrentWeatherReport.class);
        assertThat(currentWeatherReport, equalTo(currentWeatherReportExpected));
    }

    @Test
    public void coordinatesIncludedInReportGetJsonTest() throws JsonProcessingException, JSONException {
        //noinspection SpellCheckingInspection
        String jsonExpected = "{\"coord\": {\"lon\": -0.13,\"lat\": 51.51}}";
        CurrentWeatherReport currentWeatherReport = CurrentWeatherReport.make()
                .withCoordinates(
                        Coordinates.make()
                                .withLongitude(-0.13f)
                                .withLatitude(51.51f)
                                .build())
                .build();
        String json = MappingUtils.getJson(currentWeatherReport);
        JSONAssert.assertEquals(jsonExpected, json, JSONCompareMode.LENIENT);
    }

    @Test
    public void coordinatesIncludedInReportGetObjectTest() throws IOException {
        //noinspection SpellCheckingInspection
        String currentWeatherReportJson = "{\"coord\": {\"lon\": -0.13,\"lat\": 51.51}}";
        CurrentWeatherReport currentWeatherReportExpected = CurrentWeatherReport.make()
                .withCoordinates(
                        Coordinates.make()
                                .withLongitude(-0.13f)
                                .withLatitude(51.51f)
                                .build())
                .build();

        CurrentWeatherReport currentWeatherReport = MappingUtils.getObject(currentWeatherReportJson, CurrentWeatherReport.class);
        assertThat(currentWeatherReport, equalTo(currentWeatherReportExpected));
    }

    @Test
    public void weatherConditionCodesIncludedInReportGetJsonTest() throws JsonProcessingException, JSONException {
        String jsonExpected = "{\"weather\": [{\"id\": 300,\"main\": \"Drizzle\",\"description\": \"light intensity drizzle\",\"icon\": \"09d\"},{\"id\": 500,\"main\": \"Rain\",\"description\": \"light rain\",\"icon\": \"10d\"}]}";
        CurrentWeatherReport currentWeatherReport = CurrentWeatherReport.make()
                .addWeatherConditionCode(WeatherConditionCode
                        .make()
                        .withId(300)
                        .withMain("Drizzle")
                        .withDescription("light intensity drizzle")
                        .withIcon("09d")
                        .build())
                .addWeatherConditionCode(WeatherConditionCode
                        .make()
                        .withId(500)
                        .withMain("Rain")
                        .withDescription("light rain")
                        .withIcon("10d")
                        .build())
                .build();
        String json = MappingUtils.getJson(currentWeatherReport);
        JSONAssert.assertEquals(jsonExpected, json, JSONCompareMode.LENIENT);
    }

    @Test
    public void weatherConditionCodesIncludedInReportGetObjectTest() throws IOException {
        String currentWeatherReportJson = "{\"weather\": [{\"id\": 300,\"main\": \"Drizzle\",\"description\": \"light intensity drizzle\",\"icon\": \"09d\"},{\"id\": 500,\"main\": \"Rain\",\"description\": \"light rain\",\"icon\": \"10d\"}]}";
        CurrentWeatherReport currentWeatherReportExpected = CurrentWeatherReport.make()
                .addWeatherConditionCode(WeatherConditionCode
                        .make()
                        .withId(300)
                        .withMain("Drizzle")
                        .withDescription("light intensity drizzle")
                        .withIcon("09d")
                        .build())
                .addWeatherConditionCode(WeatherConditionCode
                        .make()
                        .withId(500)
                        .withMain("Rain")
                        .withDescription("light rain")
                        .withIcon("10d")
                        .build())
                .build();
        CurrentWeatherReport currentWeatherReport = MappingUtils.getObject(currentWeatherReportJson, CurrentWeatherReport.class);
        assertThat(currentWeatherReport, equalTo(currentWeatherReportExpected));
    }

    @Test
    public void baseIncludedInReportGetJsonTest() throws JsonProcessingException, JSONException {
        String jsonExpected = "{\"base\": \"stations\"}";
        CurrentWeatherReport currentWeatherReport = CurrentWeatherReport.make().withBase("stations").build();
        String json = MappingUtils.getJson(currentWeatherReport);
        JSONAssert.assertEquals(jsonExpected, json, JSONCompareMode.LENIENT);
    }

    @Test
    public void baseNameIncludedInReportGetObjectTest() throws IOException {
        String currentWeatherReportJson = "{\"base\": \"stations\"}";
        CurrentWeatherReport currentWeatherReportExpected = CurrentWeatherReport.make().withBase("stations").build();

        CurrentWeatherReport currentWeatherReport = MappingUtils.getObject(currentWeatherReportJson, CurrentWeatherReport.class);
        assertThat(currentWeatherReport, equalTo(currentWeatherReportExpected));
    }

    @Test
    public void mainWeatherIncludedInReportGetJsonTest() throws JsonProcessingException, JSONException {
        String jsonExpected = "{\"main\": {\"temp\": 7.51,\"pressure\": 1014,\"humidity\": 70,\"temp_min\": 6.11,\"temp_max\": 9}}";
        CurrentWeatherReport currentWeatherReport = CurrentWeatherReport.make()
                .withMainWeather(MainWeather.make()
                        .withTemperature(7.51f)
                        .withPressure(1014f)
                        .withHumidity(70f)
                        .withMinimumTemperatureAtTheMoment(6.11f)
                        .withMaximumTemperatureAtTheMoment(9f)
                        .build())
                .build();
        String json = MappingUtils.getJson(currentWeatherReport);
        JSONAssert.assertEquals(jsonExpected, json, JSONCompareMode.LENIENT);
    }

    @Test
    public void mainWeatherIncludedInReportGetObjectTest() throws IOException {
        String currentWeatherReportJson = "{\"main\": {\"temp\": 7.51,\"pressure\": 1014,\"humidity\": 70,\"temp_min\": 6.11,\"temp_max\": 9}}";
        CurrentWeatherReport currentWeatherReportExpected = CurrentWeatherReport.make()
                .withMainWeather(MainWeather.make()
                        .withTemperature(7.51f)
                        .withPressure(1014f)
                        .withHumidity(70f)
                        .withMinimumTemperatureAtTheMoment(6.11f)
                        .withMaximumTemperatureAtTheMoment(9f)
                        .build())
                .build();
        CurrentWeatherReport currentWeatherReport = MappingUtils.getObject(currentWeatherReportJson, CurrentWeatherReport.class);
        assertThat(currentWeatherReport, equalTo(currentWeatherReportExpected));
    }

    @Test
    public void visibilityIncludedInReportGetJsonTest() throws JsonProcessingException, JSONException {
        String jsonExpected = "{\"visibility\": 10000}";
        CurrentWeatherReport currentWeatherReport = CurrentWeatherReport.make()
                .withVisibility(10_000f)
                .build();
        String json = MappingUtils.getJson(currentWeatherReport);
        JSONAssert.assertEquals(jsonExpected, json, JSONCompareMode.LENIENT);
    }

    @Test
    public void visibilityIncludedInReportGetObjectTest() throws IOException {
        String currentWeatherReportJson = "{\"visibility\": 10000}";
        CurrentWeatherReport currentWeatherReportExpected = CurrentWeatherReport.make()
                .withVisibility(10_000f)
                .build();
        CurrentWeatherReport currentWeatherReport = MappingUtils.getObject(currentWeatherReportJson, CurrentWeatherReport.class);
        assertThat(currentWeatherReport, equalTo(currentWeatherReportExpected));
    }

    @Test
    public void windIncludedInReportGetJsonTest() throws JsonProcessingException, JSONException {
        String jsonExpected = "{\"wind\": {\"speed\": 7.2,\"deg\": 180}}";
        CurrentWeatherReport currentWeatherReport = CurrentWeatherReport.make()
                .withWind(Wind.make().withSpeed(7.2f).withDegrees(180f).build())
                .build();
        String json = MappingUtils.getJson(currentWeatherReport);
        JSONAssert.assertEquals(jsonExpected, json, JSONCompareMode.LENIENT);
    }

    @Test
    public void windIncludedInReportGetObjectTest() throws IOException {
        String currentWeatherReportJson = "{\"wind\": {\"speed\": 7.2,\"deg\": 180}}";
        CurrentWeatherReport currentWeatherReportExpected = CurrentWeatherReport.make()
                .withWind(Wind.make().withSpeed(7.2f).withDegrees(180f).build())
                .build();
        CurrentWeatherReport currentWeatherReport = MappingUtils.getObject(currentWeatherReportJson, CurrentWeatherReport.class);
        assertThat(currentWeatherReport, equalTo(currentWeatherReportExpected));
    }

    @Test
    public void cloudsIncludedInReportGetJsonTest() throws JsonProcessingException, JSONException {
        String jsonExpected = "{\"clouds\": {\"all\": 48}}";
        CurrentWeatherReport currentWeatherReport = CurrentWeatherReport.make()
                .withClouds(Clouds.make().withCloudiness(48).build())
                .build();
        String json = MappingUtils.getJson(currentWeatherReport);
        JSONAssert.assertEquals(jsonExpected, json, JSONCompareMode.LENIENT);
    }

    @Test
    public void cloudsIncludedInReportGetObjectTest() throws IOException {
        String currentWeatherReportJson = "{\"clouds\": {\"all\": 48}}";
        CurrentWeatherReport currentWeatherReportExpected = CurrentWeatherReport.make()
                .withClouds(Clouds.make().withCloudiness(48).build())
                .build();
        CurrentWeatherReport currentWeatherReport = MappingUtils.getObject(currentWeatherReportJson, CurrentWeatherReport.class);
        assertThat(currentWeatherReport, equalTo(currentWeatherReportExpected));
    }

    @Test
    public void timeOfDataCalculationIncludedInReportGetJsonTest() throws JsonProcessingException, JSONException {
        String jsonExpected = "{\"dt\": 1559190521}}";
        CurrentWeatherReport currentWeatherReport = CurrentWeatherReport.make()
                .withTimeOfDataCalculation(1559190521L)
                .build();
        String json = MappingUtils.getJson(currentWeatherReport);
        JSONAssert.assertEquals(jsonExpected, json, JSONCompareMode.LENIENT);
    }

    @Test
    public void timeOfDataCalculationIncludedInReportGetObjectTest() throws IOException {
        String currentWeatherReportJson = "{\"dt\": 1559190521}}";
        CurrentWeatherReport currentWeatherReportExpected = CurrentWeatherReport.make()
                .withTimeOfDataCalculation(1559190521L)
                .build();
        CurrentWeatherReport currentWeatherReport = MappingUtils.getObject(currentWeatherReportJson, CurrentWeatherReport.class);
        assertThat(currentWeatherReport, equalTo(currentWeatherReportExpected));
    }

    @Test
    public void systemIncludedInReportGetJsonTest() throws JsonProcessingException, JSONException {
        String jsonExpected = "{\"sys\": {\"type\": 1,\"id\": 1662,\"message\": 0.0081,\"country\": \"NO\",\"sunrise\": 1559182218,\"sunset\": 1559247282}}";
        CurrentWeatherReport currentWeatherReport = CurrentWeatherReport.make()
                .withSystem(System.make()
                        .withType(1)
                        .withId(1662)
                        .withMessage(0.0081f)
                        .withCountry("NO")
                        .withSunrise(1559182218L)
                        .withSunset(1559247282L)
                        .build())
                .build();
        String json = MappingUtils.getJson(currentWeatherReport);
        JSONAssert.assertEquals(jsonExpected, json, JSONCompareMode.LENIENT);
    }

    @Test
    public void systemIncludedInReportGetObjectTest() throws IOException {
        String currentWeatherReportJson = "{\"sys\": {\"type\": 1,\"id\": 1662,\"message\": 0.0081,\"country\": \"NO\",\"sunrise\": 1559182218,\"sunset\": 1559247282}}";
        CurrentWeatherReport currentWeatherReportExpected = CurrentWeatherReport.make()
                .withSystem(System.make()
                        .withType(1)
                        .withId(1662)
                        .withMessage(0.0081f)
                        .withCountry("NO")
                        .withSunrise(1559182218L)
                        .withSunset(1559247282L)
                        .build())
                .build();
        CurrentWeatherReport currentWeatherReport = MappingUtils.getObject(currentWeatherReportJson, CurrentWeatherReport.class);
        assertThat(currentWeatherReport, equalTo(currentWeatherReportExpected));
    }

    @Test
    public void timezoneIncludedInReportGetJsonTest() throws JsonProcessingException, JSONException {
        String jsonExpected = "{\"timezone\": 7200}";
        CurrentWeatherReport currentWeatherReport = CurrentWeatherReport.make()
                .withTimezone(7200L)
                .build();
        String json = MappingUtils.getJson(currentWeatherReport);
        JSONAssert.assertEquals(jsonExpected, json, JSONCompareMode.LENIENT);
    }

    @Test
    public void timezoneIncludedInReportGetObjectTest() throws IOException {
        String currentWeatherReportJson = "{\"timezone\": 7200}";
        CurrentWeatherReport currentWeatherReportExpected = CurrentWeatherReport.make()
                .withTimezone(7200L)
                .build();
        CurrentWeatherReport currentWeatherReport = MappingUtils.getObject(currentWeatherReportJson, CurrentWeatherReport.class);
        assertThat(currentWeatherReport, equalTo(currentWeatherReportExpected));
    }

    @Test
    public void rainIncludedInReportGetJsonTest() throws JsonProcessingException, JSONException {
        String jsonExpected = "{\"rain\": {\"rain.1h\": 11,\"rain.3h\": 25}}";
        CurrentWeatherReport currentWeatherReport = CurrentWeatherReport.make()
                .withRain(Rain.make().withRainIn1h(11).withRainIn3h(25).build())
                .build();
        String json = MappingUtils.getJson(currentWeatherReport);
        JSONAssert.assertEquals(jsonExpected, json, JSONCompareMode.LENIENT);
    }

    @Test
    public void rainIncludedInReportGetObjectTest() throws IOException {
        String currentWeatherReportJson = "{\"rain\": {\"rain.1h\": 11,\"rain.3h\": 25}}";
        CurrentWeatherReport currentWeatherReportExpected = CurrentWeatherReport.make()
                .withRain(Rain.make().withRainIn1h(11).withRainIn3h(25).build())
                .build();
        CurrentWeatherReport currentWeatherReport = MappingUtils.getObject(currentWeatherReportJson, CurrentWeatherReport.class);
        assertThat(currentWeatherReport, equalTo(currentWeatherReportExpected));
    }

    @Test
    public void snowIncludedInReportGetJsonTest() throws JsonProcessingException, JSONException {
        String jsonExpected = "{\"snow\": {\"snow.1h\": 11,\"snow.3h\": 25}}";
        CurrentWeatherReport currentWeatherReport = CurrentWeatherReport.make()
                .withSnow(Snow.make().withSnowIn1h(11).withSnowIn3h(25).build())
                .build();
        String json = MappingUtils.getJson(currentWeatherReport);
        JSONAssert.assertEquals(jsonExpected, json, JSONCompareMode.LENIENT);
    }

    @Test
    public void snowIncludedInReportGetObjectTest() throws IOException {
        String currentWeatherReportJson = "{\"snow\": {\"snow.1h\": 11,\"snow.3h\": 25}}";
        CurrentWeatherReport currentWeatherReportExpected = CurrentWeatherReport.make()
                .withSnow(Snow.make().withSnowIn1h(11).withSnowIn3h(25).build())
                .build();
        CurrentWeatherReport currentWeatherReport = MappingUtils.getObject(currentWeatherReportJson, CurrentWeatherReport.class);
        assertThat(currentWeatherReport, equalTo(currentWeatherReportExpected));
    }

    @Test
    public void messageIncludedInReportGetJsonTest() throws JsonProcessingException, JSONException {
        String jsonExpected = "{\"message\": \"city not found\"}";
        CurrentWeatherReport currentWeatherReport = CurrentWeatherReport.make()
                .withMessage("city not found")
                .build();
        String json = MappingUtils.getJson(currentWeatherReport);
        JSONAssert.assertEquals(jsonExpected, json, JSONCompareMode.LENIENT);
    }

    @Test
    public void messageIncludedInReportGetObjectTest() throws IOException {
        String currentWeatherReportJson = "{\"message\": \"city not found\"}";
        CurrentWeatherReport currentWeatherReportExpected = CurrentWeatherReport.make()
                .withMessage("city not found")
                .build();
        CurrentWeatherReport currentWeatherReport = MappingUtils.getObject(currentWeatherReportJson, CurrentWeatherReport.class);
        assertThat(currentWeatherReport, equalTo(currentWeatherReportExpected));
    }


    @Test
    public void currentWeatherReportGetObjectTest() throws IOException {
        CurrentWeatherReport currentWeatherReport = MappingUtils.getObject(getExampleResponse(), CurrentWeatherReport.class);
        assertThat(currentWeatherReport, equalTo(getExampleObject()));
    }

    private CurrentWeatherReport getExampleObject() {
        //noinspection SpellCheckingInspection
        return CurrentWeatherReport.make()
                .withCoordinates(Coordinates.make().withLongitude(11.29f).withLatitude(59.74f).build())
                .addWeatherConditionCode(WeatherConditionCode.make()
                        .withId(300)
                        .withMain("Drizzle")
                        .withDescription("light intensity drizzle")
                        .withIcon("09d")
                        .build())
                .addWeatherConditionCode(WeatherConditionCode.make()
                        .withId(500)
                        .withMain("Rain")
                        .withDescription("light rain")
                        .withIcon("10d")
                        .build())
                .withBase("stations")
                .withMainWeather(MainWeather.make()
                        .withTemperature(7.51f)
                        .withPressure(1014f)
                        .withHumidity(70f)
                        .withMinimumTemperatureAtTheMoment(6.11f)
                        .withMaximumTemperatureAtTheMoment(9f)
                        .build())
                .withVisibility(10_000f)
                .withWind(Wind.make().withSpeed(7.2f).withDegrees(180f).build())
                .withClouds(Clouds.make().withCloudiness(48).build())
                .withTimeOfDataCalculation(1559190521L)
                .withSystem(System.make()
                        .withType(1)
                        .withId(1662)
                        .withMessage(0.0081f)
                        .withCountry("NO")
                        .withSunrise(1559182218L)
                        .withSunset(1559247282L)
                        .build())
                .withTimezone(7200L)
                .withCityId(7626324L)
                .withCityName("Kirkebygda")
                .withCod(200L)
                .build();
    }

    private String getExampleResponse() {
        //noinspection SpellCheckingInspection
        return "{\n" +
                "    \"coord\": {\n" +
                "        \"lon\": 11.29,\n" +
                "        \"lat\": 59.74\n" +
                "    },\n" +
                "    \"weather\": [\n" +
                "        {\n" +
                "            \"id\": 300,\n" +
                "            \"main\": \"Drizzle\",\n" +
                "            \"description\": \"light intensity drizzle\",\n" +
                "            \"icon\": \"09d\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": 500,\n" +
                "            \"main\": \"Rain\",\n" +
                "            \"description\": \"light rain\",\n" +
                "            \"icon\": \"10d\"\n" +
                "        }\n" +
                "    ],\n" +
                "    \"base\": \"stations\",\n" +
                "    \"main\": {\n" +
                "        \"temp\": 7.51,\n" +
                "        \"pressure\": 1014,\n" +
                "        \"humidity\": 70,\n" +
                "        \"temp_min\": 6.11,\n" +
                "        \"temp_max\": 9\n" +
                "    },\n" +
                "    \"visibility\": 10000," +
                "    \"wind\": {\n" +
                "        \"speed\": 7.2,\n" +
                "        \"deg\": 180\n" +
                "    },\n" +
                "    \"clouds\": {\n" +
                "        \"all\": 48\n" +
                "    },\n" +
                "    \"dt\": 1559190521,\n" +
                "    \"sys\": {\n" +
                "        \"type\": 1,\n" +
                "        \"id\": 1662,\n" +
                "        \"message\": 0.0081,\n" +
                "        \"country\": \"NO\",\n" +
                "        \"sunrise\": 1559182218,\n" +
                "        \"sunset\": 1559247282\n" +
                "    },\n" +
                "    \"timezone\": 7200,\n" +
                "    \"id\": 7626324,\n" +
                "    \"name\": \"Kirkebygda\",\n" +
                "    \"cod\": 200\n" +
                "}";
    }

}

